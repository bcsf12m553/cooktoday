﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cooktoday.Models;

namespace Cooktoday.Controllers
{
    public class UserController : Controller
    {


        DB_CookTodayEntities mde = new DB_CookTodayEntities();
        public ActionResult Index()
        {
           
            return View(mde.Categories.ToList());
        }

        

        public ActionResult AddRecipetoDB(Cooktoday.Models.Recipe p)
        {

              
            mde.Categories.Where(x => x.Category1 == p.Category);

            mde.Recipes.Add(p);

            mde.SaveChanges();


            return Redirect("/User/Index");

        }


        public ActionResult UploadedRecipes()
        {
            //mde.Products.Where(x => x.Price >= 0 && x.Price <= r).ToList()
            //return View(mde.Recipes.Where();

            string n = (string)Session["uname"];


           List<Recipe> list = mde.Recipes.Where(x => x.uploaded_by == n).ToList() ;

            return View(list);
        }

        public ActionResult DeleteRecipe(Cooktoday.Models.Recipe p)
        {
            Recipe si = mde.Recipes.Where(x => x.r_name == p.r_name).SingleOrDefault();
            mde.Recipes.Remove(si);
            mde.SaveChanges();

            return Redirect("/User/UploadedRecipes");
        }



    }
}
