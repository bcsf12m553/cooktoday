﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cooktoday.Models;


namespace Cooktoday.Controllers
{
    public class AdminController : Controller
    {
        //
        // GET: /Admin/

        DB_CookTodayEntities mde = new DB_CookTodayEntities();
        public ActionResult Index()
        {
            return Redirect("/Home/Index");
        }
        


        public ActionResult Adminlogin()
        {

            return View();

        }

        public ActionResult AddCategory()
        {
            return View(mde.Categories.ToList());

        }

        public ActionResult AddCategoryToDB(Cooktoday.Models.Category p)
        {


            mde.Categories.Add(p);
            mde.SaveChanges();


            return RedirectToAction("AddCategory");
        }

   

    }
}
