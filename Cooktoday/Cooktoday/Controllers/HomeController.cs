﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cooktoday.Models;
using System.Data.Entity;



namespace MvcApplication1.Controllers
{
   
    public class HomeController : Controller
    {

        

        DB_CookTodayEntities mdi = new DB_CookTodayEntities();


        public ActionResult Index()
        {


            return View(mdi.Categories.ToList());


        }

        public ActionResult about()
        {
            return View(mdi.Categories.ToList());

        }

        public ActionResult contact()
        {

            return View(mdi.Categories.ToList());
        }

        public ActionResult newrecipes()
        {
            return View(mdi.Recipes.ToList());

        }

        public ActionResult news()
        {

            return View(mdi.Categories.ToList());
        }

        public ActionResult chef()
        {

            return View(mdi.Categories.ToList());
        }



        public ActionResult desc()
        {

            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LoginAdmin(Cooktoday.Models.AdminLogin login)
        {
            Admin si = mdi.Admins.Where(x => x.Email == login.Email).SingleOrDefault();
            if (si != null)
            {
                if ((si.Email == login.Email) && (si.Password == login.Password))
                {
                    Session["Admin"] = login.Email;
                    return Redirect("/Admin/AddCategory");

                }
                else
                    ViewBag.Error1 = "Login Error: Invalid Username or password";
            }

            else
                ViewBag.Error1 = "Login Error: Invalid Username or password";
            return Redirect("/Admin/AdminLogin");


        }

        public ActionResult Signin(Cooktoday.Models.user p)
        {

            user si = mdi.users.Where(x => x.username == p.username).SingleOrDefault();
            if (si != null)
            {
                if ((si.username == p.username) && (si.password == p.password))
                {
                    Session["uname"] = p.username;

                    return Redirect("/User/Index");

                }
                else
                {

                    ViewBag.Error = " Incorrect Username and Password";
                    return View("login");
                }
            }
            else
                ViewBag.Error = " Incorrect Username and Password";
            return View("login");


        }

        public ActionResult login()
        {

            return View();

        }

        
        public ActionResult signup()
        {
            return View();
        }

        public ActionResult SignupValidate(Cooktoday.Models.user p)
        {

            user si = mdi.users.Where(x => x.username == p.username).SingleOrDefault();
            if (si == null)
            {
                mdi.users.Add(p);
                mdi.SaveChanges();
                Session["uname"] = p.username;
                return Redirect("/User/Index");
            }
            else
            {
                ViewBag.Error = "Username Already Exists ......";
                return View("signup");
            }
        }

        public ActionResult logout()
        {
            Session["Admin"] = null;
            Session["uname"] = null;
            return Redirect("/Home/Index");

        }
   







    }
}
