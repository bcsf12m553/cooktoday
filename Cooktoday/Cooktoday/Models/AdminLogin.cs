﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Cooktoday.Models
{
    public class AdminLogin
    {


        public int Id { get; set; }

        [Required(ErrorMessage = "Name field is required")]
        [Display(Name = "Enter mail")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "Password")]
        [Required(ErrorMessage = "Password field is required")]
        [DataType(DataType.Password)]

        public string Password { get; set; }


    }
}